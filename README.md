# eventuous-tests
Web application implemented with [Eventuous](https://eventuous.dev/) which uses event sourcing as persistence mechanism with Event Store DB, and catch-up subscriptions for read models with Event Store DB and Mongo DB.

## Requirements
- SDK .NET 7
- Event Store DB (as a Docker container. See below)
- Mongo DB (as a Docker container. See below)
- Hosts file with the following entries (`/etc/hosts` in Unix based OS and at `C:\Windows\System32\drivers\etc\hosts` in Windows.) to give alias to Event Store DB and Mongo DB in local as they have in GitLab CI/CD services
  ```
  # Local servers
  127.0.0.1   eventstoredb
  127.0.0.1   mongodb
  ```
  
### Event Store DB
Spin up an Event Store DB container (in-memory persistence) with
```
docker run \
    --name esdb \
    -p 2113:2113 \
    -p 1113:1113 \
    eventstore/eventstore:22.10.0-buster-slim \
    --insecure \
    --enable-atom-pub-over-http \
    --mem-db=True
```

### Mongo DB
Spin up a Mongo DB container (in-memory persistence) with
```
docker run \
    --name mongo \
    --mount type=tmpfs,destination=/data/db \
    -e MONGO_INITDB_ROOT_USERNAME=root \
    -e MONGO_INITDB_ROOT_PASSWORD=dummy \
    -p 27017:27017 \
    mongo:4.0
```
NOTE: The version is 4.0 to make it compatible with AWS DocumentDb