namespace Modular.Api;

public static class Constants
{
    public static class CatchupSubscriptions
    {
        public const string ProjectionSubscriptionId = "projections";
    }
}