using Eventuous.Projections.MongoDB;
using Eventuous.Subscriptions.Context;
using MongoDB.Driver;
using static Modular.Clients.DomainEvents.ClientEvents;

// ReSharper disable ClassNeverInstantiated.Global

namespace Modular.ClientsInfo;

public sealed class ClientProjection
    : MongoProjection<ClientDocument>
{
    public ClientProjection(IMongoDatabase database) 
        : base(database)
    {
        On<V1.ClientCreated>(stream => stream.GetId(), Handler);
    }

    private UpdateDefinition<ClientDocument> Handler(
        IMessageConsumeContext<V1.ClientCreated> ctx, 
        UpdateDefinitionBuilder<ClientDocument> update)
    {
        var clientCreated = ctx.Message;
        var clientId = ctx.Stream.GetId();

        var updateDefinition =
            update
                .SetOnInsert(x => x.Id, clientId)
                .Set(x => x.Name, clientCreated.DisplayName)
                .Set(x => x.AdminEmail, clientCreated.AdminEmail)
                .Set(x => x.CreatedOn, ctx.Created);
        
        return updateDefinition;
    }
}