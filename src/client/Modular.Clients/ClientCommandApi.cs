using Eventuous;
using Eventuous.AspNetCore.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Modular.Clients.Domain;
using static Modular.Clients.Commands.ClientCommands;

namespace Modular.Clients;

[Route("clients")]
public class ClientCommandApi
    : CommandHttpApiBase<Client>
{
    public ClientCommandApi(IApplicationService<Client> service) 
        : base(service)
    {
    }

    [AllowAnonymous]
    [HttpPost]
    [Route("create")]
    public Task<ActionResult<Result>> CreateClient(
        [FromBody] CreateClient createClient,
        CancellationToken cancellationToken)
        => Handle(createClient, cancellationToken);
    
    [AllowAnonymous]
    [HttpPost]
    [Route("updateAdminCredentials")]
    public Task<ActionResult<Result>> UpdateAdminCredentials(
        [FromBody] UpdateClientAdminCredentials updateClientAdminCredentials,
        CancellationToken cancellationToken)
        => Handle(updateClientAdminCredentials, cancellationToken);
}