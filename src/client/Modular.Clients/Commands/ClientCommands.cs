namespace Modular.Clients.Commands;

public static class ClientCommands
{
    /// <summary>
    /// Creates a client. The Id and client Id are the same
    /// </summary>
    /// <param name="Id">the client's unique identifier (i.e: aggregate root Id)</param>
    /// <param name="DisplayName">the client's display name</param>
    /// <param name="AdminEmail">the client administrator's email</param>
    public record CreateClient(
        string Id,
        string DisplayName,
        string AdminEmail);
    
    /// <summary>
    /// Sets the administrator credentials. The Id and client Id are the same
    /// </summary>
    /// <param name="Id">the client's unique identifier (i.e: aggregate root Id)</param>
    /// <param name="Password">the administrator's password</param>
    public record UpdateClientAdminCredentials(
        string Id,
        string Password);
}